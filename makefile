redeploy: build stop
	docker run -d --restart unless-stopped -p 80:5000 --name flask-card-trick flask-card-trick:latest

build:
	docker build -t flask-card-trick:latest .

stop:
	docker stop flask-card-trick && docker rm flask-card-trick || true
