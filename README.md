# Flask card trick

## Background
I used this as part of a little card trick for a friend's birthday.

## About
This is a simple Flask server that displays a [unicode playing card](https://en.wikipedia.org/wiki/Playing_cards_in_Unicode) (see table below) which can be preset or updated. The default card is a card back (🂠).

## Usage
1. Requesting the route `/` will display the current card (default is a blank card back).
2. The card can be set by requesting the route `/u<new_card>` (`u` is short for update), where `new_card` is a case-insensitive card name consisting of an index and suit (see table below).

   Example values: `3c` (for _3 of Clubs_), `10d` (for _10 of Diamonds_) or `as` (for _Ace of Spades_).
3. The 2nd step can be repeated as many times as desired.
4. Requesting the route `/r` will reset to the blank card back.

### Unicode cards
| `new_card`       | `a` (_Ace_) | `2` | `3` | `4` | `5` | `6` | `7` | `8` | `9` | `10` or `0` | `j` (_Jack_) | `q` (_Queen_) | `k` (_King_) |
|------------------|-------------|-----|-----|-----|-----|-----|-----|-----|-----|-------------|--------------|---------------|--------------|
| `s` (_Spades_)   | 🂡 | 🂢 | 🂣 | 🂤 | 🂥 | 🂦 | 🂧 | 🂨 | 🂩 | 🂪 | 🂫 | 🂭 | 🂮 |
| `h` (_Hearts_)   | 🂱 | 🂲 | 🂳 | 🂴 | 🂵 | 🂶 | 🂷 | 🂸 | 🂹 | 🂺 | 🂻 | 🂽 | 🂾 |
| `d` (_Diamonds_) | 🃁 | 🃂 | 🃃 | 🃄 | 🃅 | 🃆 | 🃇 | 🃈 | 🃉 | 🃊 | 🃋 | 🃍 | 🃎 |
| `c` (_Clubs_)    | 🃑 | 🃒 | 🃓 | 🃔 | 🃕 | 🃖 | 🃗 | 🃘 | 🃙 | 🃚 | 🃛 | 🃝 | 🃞 |
